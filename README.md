# MovieCharactersAPI

This is our submission for the third assignment in the Noroff Accelerate .NET course, made by Edvard Lindgren and Emil Onsøyen.

The goal of the assignment is to create a Web API.

## Appendix A:
Use Entity Framework with a Code First approach to create a database with seeded data.

## Appendix B:
Create a Web API in <span>ASP.NET</span> Core with controllers, DTOs and services/repositories.

## Project Tree
```bash
└───MovieCharactersAPI              # ASP.NET Core Web API application
    └───MovieCharactersAPI
        ├───bin
        │   └───Debug
        │       └───net5.0
        ├───Controllers             # Controllers
        ├───DataAccess              # Repositories
        │   ├───Character
        │   ├───Franchise
        │   └───Movie
        ├───Migrations              # Migrations for creating and seeding Db
        ├───Models
        │   ├───Domain              # Domain models
        │   └───DTOs                # DTO models
        │       ├───Character
        │       ├───Franchise
        │       └───Movie
        ├───obj
        │   └───Debug
        │       └───net5.0
        ├───Profiles                # DTO Mapping profiles
        └───Properties
```


