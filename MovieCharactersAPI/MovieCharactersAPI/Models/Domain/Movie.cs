﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models.Domain
{
	[Table("Movie")]
	public class Movie
	{
		//Primary Key
		public int Id { get; set; }
		[Required]
		[MaxLength(255)]
		public string MovieTitle { get; set; }
		public string Genre { get; set; }
		public int ReleaseYear { get; set; }
		public string Director { get; set; }
		public string Picture { get; set; }
		public string Trailer { get; set; }
		public int FranchiseId { get; set; }
		public Franchise Franchise { get; set; }
		public ICollection<MovieCharacter> MovieCharacters { get; set; }
	}

}

