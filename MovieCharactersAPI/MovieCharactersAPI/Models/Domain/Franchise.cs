﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models.Domain
{
	[Table("Franchise")]
	public class Franchise
	{
		//PrimaryKey
		public int Id { get; set; }
		[Required]
		[MaxLength(255)]
		public string Name { get; set; }
		[MaxLength(500)]
		public string Description { get; set; }
		public ICollection<Movie> Movies { get; set; }
	}
}

