﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models.Domain
{
	[Table("MovieCharacter")]
	public class MovieCharacter
	{
		//Linking table between Movie and Character
		public int MovieId { get; set; }
		public Movie Movie { get; set; }
		public int CharacterId { get; set; }
		public Character Character { get; set; }
	}
}
