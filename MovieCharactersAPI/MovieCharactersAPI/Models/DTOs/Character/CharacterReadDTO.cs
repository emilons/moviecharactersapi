﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models.DTOs.Character
{
	/// <summary>
	/// Variables for CharacterReadDTO
	/// </summary>
    public class CharacterReadDTO
    {
        public int Id { get; set; }
		public string FullName { get; set; }
		public string Profession { get; set; }
		public string Alias { get; set; }
		public string Gender { get; set; }
		public string Picture { get; set; }
		public List<int> Movies { get; set; }

	}
}
