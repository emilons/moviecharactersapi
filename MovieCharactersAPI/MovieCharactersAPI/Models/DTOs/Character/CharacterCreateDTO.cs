﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models.DTOs.Character
{
    public class CharacterCreateDTO
    {
		/// <summary>
		/// Variables for CharacterCreateDTO
		/// </summary>
		public string FullName { get; set; }
		public string Profession { get; set; }
		public string Alias { get; set; }
		public string Gender { get; set; }
		public string Picture { get; set; }
		public List<int> Movies { get; set; }
	}
}
