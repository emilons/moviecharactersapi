﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models.DTOs.Franchise
{
	/// <summary>
	/// Variables for FranchiseCreateDTO
	/// </summary>
    public class FranchiseCreateDTO
    {
		public string Name { get; set; }
		public string Description { get; set; }
	}
}
