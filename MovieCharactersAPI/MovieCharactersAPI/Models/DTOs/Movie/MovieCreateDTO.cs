﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models.DTOs.Movie
{
	/// <summary>
	/// Variables for MovieCreateDTO
	/// </summary>
    public class MovieCreateDTO
    {
        public string MovieTitle { get; set; }
		public string Genre { get; set; }
		public int ReleaseYear { get; set; }
		public string Director { get; set; }
		public string Picture { get; set; }
		public string Trailer { get; set; }
		public int Franchise { get; set; }
    }
}
