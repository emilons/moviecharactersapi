﻿using AutoMapper;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTOs.Character;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MovieCharactersAPI.Models.Domain;

namespace MovieCharactersAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            //Mapping from character to CharacterReadDTO -> Find MovieID from character
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.MovieCharacters.Select(mc => mc.MovieId).ToArray()))
                .ReverseMap();

            //Mapping from character to CharacterReadDTO -> Find MovieID from character
            CreateMap<Character, CharacterCreateDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.MovieCharacters.Select(mc => mc.MovieId).ToArray()))
                .ReverseMap();

            //Mapping from character to CharacterReadDTO -> Find MovieID from character
            CreateMap<Character, CharacterEditDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.MovieCharacters.Select(mc => mc.MovieId).ToArray()))
                .ReverseMap();



        }
    }
}
