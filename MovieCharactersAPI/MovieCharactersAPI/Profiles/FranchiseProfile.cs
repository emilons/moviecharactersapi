﻿using AutoMapper;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTOs.Franchise;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MovieCharactersAPI.Models.Domain;

namespace MovieCharactersAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            //Mapping from Franchise to FranchiseReadDTO
            CreateMap<Franchise, FranchiseReadDTO>()
                .ReverseMap();

            //Mapping from Franchise to FranchiseReadDTO
            CreateMap<Franchise, FranchiseCreateDTO>()
                .ReverseMap();

            //Mapping from Franchise to FranchiseReadDTO
            CreateMap<Franchise, FranchiseEditDTO>()
                .ReverseMap();
        }
    }
}
