﻿using AutoMapper;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTOs.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MovieCharactersAPI.Models.Domain;

namespace MovieCharactersAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            //Map between Movie and MovieReadDTO -- Map Franchise to Franchise id -- Find CharacterID from Movie
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.Characters, opt=>opt
                .MapFrom(m=>m.MovieCharacters.Select(mc=>mc.CharacterId).ToArray()))
                .ForMember(mdto => mdto.Franchise, opt => opt
                .MapFrom(m => m.FranchiseId))
                .ReverseMap();


            // Map between Movie and MovieCreateDTO -- Map Franchise to Franchise id
            CreateMap<Movie, MovieCreateDTO>()
                .ForMember(mdto => mdto.Franchise, opt => opt
                .MapFrom(m => m.FranchiseId))
                .ReverseMap();

            // Map between Movie and MovieEditDTO -- Map Franchise to Franchise id
            CreateMap<Movie, MovieEditDTO>()
                .ForMember(mdto => mdto.Franchise, opt => opt
                .MapFrom(m => m.FranchiseId))
                .ReverseMap();

        }
    }
}
