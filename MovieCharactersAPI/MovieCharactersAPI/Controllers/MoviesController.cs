﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.DataAccess;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTOs.Movie;
using MovieCharactersAPI.Models.DTOs.Character;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/v1/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IMapper _mapper;

        public MoviesController(IMovieRepository movieRepository, IMapper mapper)
        {
            _movieRepository = movieRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all movies in Db.
        /// </summary>
        /// <returns>MovieReadDTO object.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper
                .Map<List<MovieReadDTO>> (await _movieRepository
                .GetMoviesAsync());
        }

        /// <summary>
        /// Get specific Movie in Db.
        /// </summary>
        /// <param name="id">Id of Movie object.</param>
        /// <returns>MovieReadDTO object.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movie = await _movieRepository.GetMovieAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper
                .Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Get list of characters in a Movie.
        /// </summary>
        /// <param name="id">Id of Movie object.</param>
        /// <returns>List of CharacterReadDTOs.</returns>
        [HttpGet("GetCharacters/{id}")]
        public async Task<ActionResult<List<CharacterReadDTO>>> GetCharactersInMovie(int id)
        {
            var movie = await _movieRepository.GetMovieAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper
                .Map<List<CharacterReadDTO>>(await _movieRepository
                .GetCharactersInMovie(id));
        }

        /// <summary>
        /// Put specific Movie, otherwise update Movie in Db.
        /// </summary>
        /// <param name="id">Id of Movie object.</param>
        /// <param name="movieDto">MovieEditDTO object.</param>
        /// <returns>HTTP status message.</returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO movieDto)
        {
            if (id != movieDto.Id)
            {
                return BadRequest();
            }

            if (!_movieRepository.MovieExists(id))
            {
                return NotFound();
            }

            Movie domainMovie = _mapper.Map<Movie>(movieDto);
            await _movieRepository.PutMovieAsync(id, domainMovie);

            return NoContent();
        }

        /// <summary>
        /// Put a Movie's list of characters, otherwise update Movie with list of characters in Db.
        /// </summary>
        /// <param name="id">Id of Movie Object.</param>
        /// <param name="characterIds">List of character ids.</param>
        /// <returns>HTTP status message.</returns>
        [HttpPut("UpdateCharacters/{id}")]
        public async Task<IActionResult> PutCharactersInMovie(int id, List<int> characterIds)
        {
            if (!_movieRepository.MovieExists(id))
            {
                return NotFound();
            }

            bool isGoodRequest = await _movieRepository.PutCharactersInMovie(id, characterIds);
            if (!isGoodRequest)
            {
                return BadRequest();
            }

            return NoContent();
        }

        /// <summary>
        /// Post movie, add new Movie to Db.
        /// </summary>
        /// <param name="dtoMovie">MovieCreateDTO object.</param>
        /// <returns>HTTP status message 201 Created.</returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(MovieCreateDTO dtoMovie)
        {
            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);
            await _movieRepository.PostMovieAsync(domainMovie);

            return CreatedAtAction("GetMovie",
                new { id = domainMovie.Id },
                _mapper.Map<MovieReadDTO>(domainMovie));
        }

        /// <summary>
        /// Delete Movie from Db.
        /// </summary>
        /// <param name="id">Id of Movie object.</param>
        /// <returns>HTTP status message.</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieRepository.MovieExists(id))
            {
                return NotFound();
            }

            await _movieRepository.DeleteMovieAsync(id);

            return NoContent();
        }
    }
}
