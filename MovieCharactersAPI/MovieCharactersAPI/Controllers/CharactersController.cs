﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.DataAccess;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTOs.Character;
using System.Net.Mime;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/v1/characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly ICharacterRepository _characterRepository;
        private readonly IMapper _mapper;

        public CharactersController(ICharacterRepository characterRepository, IMapper mapper)
        {
            _characterRepository = characterRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all characters in Db.
        /// </summary>
        /// <returns>List of CharacterReadDTO objects.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper
                .Map<List<CharacterReadDTO>>(await _characterRepository
                .GetCharactersAsync());
        }

        /// <summary>
        /// Get specific Character in Db.
        /// </summary>
        /// <param name="id">Id of Character object.</param>
        /// <returns>CharacterReadDTO object.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _characterRepository.GetCharacterAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper
                .Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Put specific Character, update Character in Db.
        /// </summary>
        /// <param name="id">Id of Character object.</param>
        /// <param name="characterDto">CharacterEditDTO object.</param>
        /// <returns>HTTP status message.</returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO characterDto)
        {
            if (id != characterDto.Id)
            {
                return BadRequest();
            }

            if (!_characterRepository.CharacterExists(id))
            {
                return NotFound();
            }

            Character domainCharacter = _mapper.Map<Character>(characterDto);
            await _characterRepository.PutCharacterAsync(id, domainCharacter);

            return NoContent();
        }

        /// <summary>
        /// Post Character, add new Character to Db.
        /// </summary>
        /// <param name="dtoCharacter">CharacterCreateDTO object.</param>
        /// <returns>HTTP status message 201 Created.</returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(CharacterCreateDTO dtoCharacter)
        {
            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);
            
            await _characterRepository.PostCharacterAsync(domainCharacter);
            
            return CreatedAtAction("GetCharacter",
                new { id = domainCharacter.Id },
                _mapper.Map<CharacterReadDTO>(domainCharacter));
        }

        /// <summary>
        /// Delete Character from Db.
        /// </summary>
        /// <param name="id">Id of Character object.</param>
        /// <returns>HTTP status message.</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (!_characterRepository.CharacterExists(id))
            {
                return NotFound();
            }

            await _characterRepository.DeleteCharacterAsync(id);

            return NoContent();
        }
    }
}
