﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.DataAccess;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.DTOs.Franchise;
using MovieCharactersAPI.Models.DTOs.Movie;
using MovieCharactersAPI.Models.DTOs.Character;
using System.Net.Mime;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/v1/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly IFranchiseRepository _franchiseRepository;
        private readonly IMapper _mapper;

        public FranchisesController(IFranchiseRepository franchiseRepository, IMapper mapper)
        {
            _franchiseRepository = franchiseRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all franchises in Db.
        /// </summary>
        /// <returns>List of FranchiseReadDTO objects.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper
                .Map<List<FranchiseReadDTO>>(await _franchiseRepository
                .GetFranchisesAsync());
        }

        /// <summary>
        /// Get specific Franchise in Db.
        /// </summary>
        /// <param name="id">Id of Franchise object.</param>
        /// <returns>FranchiseReadDTO object.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _franchiseRepository.GetFranchiseAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        /// <summary>
        /// Get all movies in a specific Franchise.
        /// </summary>
        /// <param name="id">Id of Franchise object.</param>
        /// <returns>List of MovieReadDTO objects.</returns>
        [HttpGet("GetMovies/{id}")]
        public async Task<ActionResult<List<MovieReadDTO>>> GetMoviesInFranchise(int id)
        {
            var franchise = await _franchiseRepository.GetFranchiseAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.
                Map<List<MovieReadDTO>>(await _franchiseRepository
                .GetMoviesInFranchise(id));
        }

        /// <summary>
        /// Get all characters in a specific Franchise.
        /// </summary>
        /// <param name="id">Id of Franchise object.</param>
        /// <returns>List of CharacterReadDTO objects.</returns>
        [HttpGet("GetCharacters/{id}")]
        public async Task<ActionResult<List<CharacterReadDTO>>> GetCharactersInFranchise(int id)
        {
            var franchise = await _franchiseRepository.GetFranchiseAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.
                Map<List<CharacterReadDTO>>(await _franchiseRepository
                .GetCharactersInFranchise(id));
        }

        /// <summary>
        /// Put specific Franchise, update Franchise in Db.
        /// </summary>
        /// <param name="id">Id of Franchise object.</param>
        /// <param name="franchiseDto">FranchiseEditDTO object.</param>
        /// <returns>HTTP status message.</returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO franchiseDto)
        {
            if (id != franchiseDto.Id)
            {
                return BadRequest();
            }
            
            if (!_franchiseRepository.FranchiseExists(id))
            {
                return NotFound();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(franchiseDto);
            await _franchiseRepository.PutFranchiseAsync(id, domainFranchise);

            return NoContent();
        }

        /// <summary>
        /// Put a Franchise's list of movies, update Franchise with list of movies in Db.
        /// </summary>
        /// <param name="id">Id of Franchise object.</param>
        /// <param name="movieIds">List of movie ids.</param>
        /// <returns>HTTP status message.</returns>
        [HttpPut("UpdateMovies/{id}")]
        public async Task<IActionResult> PutMoviesInFranchise(int id, List<int> movieIds)
        {
            if (!_franchiseRepository.FranchiseExists(id))
            {
                return NotFound();
            }

            bool isGoodRequest = await _franchiseRepository.PutMoviesInFranchise(id, movieIds);
            if (!isGoodRequest)
            {
                return BadRequest();
            }

            return NoContent();
        }

        /// <summary>
        /// Post Franchise, add new Franchise to Db.
        /// </summary>
        /// <param name="dtoFranchise">FranchiseCreateDTO object.</param>
        /// <returns>HTTP status message 201 Created.</returns>
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseCreateDTO dtoFranchise)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);

            await _franchiseRepository.PostFranchiseAsync(domainFranchise);
            
            return CreatedAtAction("GetFranchise", 
                new { id = domainFranchise.Id},
                _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        /// <summary>
        /// Delete Franchise from Db.
        /// </summary>
        /// <param name="id">Id of Franchise object.</param>
        /// <returns>HTTP status message.</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_franchiseRepository.FranchiseExists(id))
            {
                return NotFound();
            }

            await _franchiseRepository.DeleteFranchiseAsync(id);

            return NoContent();
        }
    }
}
