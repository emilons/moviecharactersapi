﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieCharactersAPI.Migrations
{
    public partial class Seeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Character",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "Picture", "Profession" },
                values: new object[,]
                {
                    { 1, null, "Luke Skywalker", "Male", "https://en.wikipedia.org/wiki/Luke_Skywalker#/media/File:Luke_Skywalker.png", "Padawan" },
                    { 2, null, "Leia Organa", "Female", "https://upload.wikimedia.org/wikipedia/en/1/1b/Princess_Leia%27s_characteristic_hairstyle.jpg", "Princess" },
                    { 3, null, "Ethan Hunt", "Male", "https://upload.wikimedia.org/wikipedia/en/f/f2/Ethan_Hunt_MI.jpg", "Agent" },
                    { 4, null, "Bella Swan", "Female", "https://upload.wikimedia.org/wikipedia/en/7/77/Bella_Swan.jpg", "Student" }
                });

            migrationBuilder.InsertData(
                table: "Franchise",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "A long time ago in a galaxy far far away...", "Star Wars" },
                    { 2, "DUN DUN DUNDUN DUN DUN DUNDUN, DIDALEEEEEEEEEEEEE, DIDALEEEEEEE, DIDALEEEEEE, DADAH!", "Mission Impossible" },
                    { 3, "Teenagers, vampires and werevoles and love triangles and stuff.", "The Twilight Saga" }
                });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "MovieTitle", "Picture", "ReleaseYear", "Trailer" },
                values: new object[,]
                {
                    { 1, "George Lucas", 1, "Sci-Fi", "Star Wars: Episode IV - A New Hope", "https://www.imdb.com/title/tt0076759/mediaviewer/rm3263717120/", 1977, "https://www.youtube.com/watch?v=vZ734NWnAHA" },
                    { 2, "Irvin Kershner", 1, "Sci-Fi", "Star Wars: Episode V: The Empire Strikes Back", "https://www.imdb.com/title/tt0080684/mediaviewer/rm3114097664/", 1980, "https://www.youtube.com/watch?v=JNwNXF9Y6kY" },
                    { 3, "Richard Marquand", 1, "Sci-Fi", "Star Wars: Episode VI: Return of the Jedi", "https://www.imdb.com/title/tt0086190/mediaviewer/rm602420224/", 1983, "https://www.youtube.com/watch?v=7L8p7_SLzvU" },
                    { 4, "Brian De Palma", 2, "Action", "Mission: Impossible", "https://www.imdb.com/title/tt0117060/mediaviewer/rm3243234304/", 1996, "https://www.youtube.com/watch?v=Ohws8y572KE" },
                    { 5, "Catherine Hardwicke", 3, "Fantasy", "Twilight", "https://www.imdb.com/title/tt1099212/mediaviewer/rm2266076160/", 2008, "https://www.youtube.com/watch?v=uxjNDE2fMjI" }
                });

            migrationBuilder.InsertData(
                table: "MovieCharacter",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 1, 2 },
                    { 2, 2 },
                    { 1, 3 },
                    { 2, 3 },
                    { 3, 4 },
                    { 4, 5 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "MovieCharacter",
                keyColumns: new[] { "CharacterId", "MovieId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "MovieCharacter",
                keyColumns: new[] { "CharacterId", "MovieId" },
                keyValues: new object[] { 2, 1 });

            migrationBuilder.DeleteData(
                table: "MovieCharacter",
                keyColumns: new[] { "CharacterId", "MovieId" },
                keyValues: new object[] { 1, 2 });

            migrationBuilder.DeleteData(
                table: "MovieCharacter",
                keyColumns: new[] { "CharacterId", "MovieId" },
                keyValues: new object[] { 2, 2 });

            migrationBuilder.DeleteData(
                table: "MovieCharacter",
                keyColumns: new[] { "CharacterId", "MovieId" },
                keyValues: new object[] { 1, 3 });

            migrationBuilder.DeleteData(
                table: "MovieCharacter",
                keyColumns: new[] { "CharacterId", "MovieId" },
                keyValues: new object[] { 2, 3 });

            migrationBuilder.DeleteData(
                table: "MovieCharacter",
                keyColumns: new[] { "CharacterId", "MovieId" },
                keyValues: new object[] { 3, 4 });

            migrationBuilder.DeleteData(
                table: "MovieCharacter",
                keyColumns: new[] { "CharacterId", "MovieId" },
                keyValues: new object[] { 4, 5 });

            migrationBuilder.DeleteData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Character",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Franchise",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Franchise",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Franchise",
                keyColumn: "Id",
                keyValue: 3);
        }
    }
}
