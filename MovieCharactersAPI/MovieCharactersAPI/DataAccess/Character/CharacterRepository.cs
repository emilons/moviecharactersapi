﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models.Domain;

namespace MovieCharactersAPI.DataAccess
{
    public class CharacterRepository : ICharacterRepository
    {
        private readonly MovieDbContext _context;

        public CharacterRepository(MovieDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Delete Character from Db.
        /// </summary>
        /// <param name="id">Id of Character object.</param>
        /// <returns>Async operation.</returns>
        public async Task DeleteCharacterAsync(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Get Character from Db
        /// </summary>
        /// <param name="id">Id of Character object</param>
        /// <returns>Async operation</returns>
        public async Task<Character> GetCharacterAsync(int id)
        {
            var characterList = await GetCharactersAsync();
            var character = characterList.FirstOrDefault(c => c.Id == id);

            return character;
        }

        /// <summary>
        /// Get Characters from Db.
        /// </summary>
        /// <returns>Async operation</returns>
        public async Task<IEnumerable<Character>> GetCharactersAsync()
        {
            return await _context.Characters
            .Include(c => c.MovieCharacters)
            .ToListAsync();
        }

        /// <summary>
        /// Post Character
        /// </summary>
        /// <param name="character">Name of character object</param>
        /// <returns>Async operation</returns>
        public async Task<Character> PostCharacterAsync(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
            return character;
        }

        /// <summary>
        /// Put Character
        /// </summary>
        /// <param name="id">Id of character object</param>
        /// <param name="character">Name of character object</param>
        /// <returns>Async operation</returns>
        public async Task PutCharacterAsync(int id, Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }
    }
}
