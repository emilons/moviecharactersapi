﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models.Domain;

namespace MovieCharactersAPI.DataAccess
{
    public class FranchiseRepository : IFranchiseRepository
    {
        private readonly MovieDbContext _context;

        public FranchiseRepository(MovieDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Delete Franchize from Db.
        /// </summary>
        /// <param name="id">Id of Franchise object</param>
        /// <returns>Async operation.</returns>
        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        public bool FranchiseExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }

        /// <summary>
        /// Get Characters in a Franchise
        /// </summary>
        /// <param name="id">Id of the Franchise</param>
        /// <returns>Async operation.</returns>
        public async Task<List<Character>> GetCharactersInFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);

            IEnumerable<Character> characters = await _context.Characters.ToListAsync();
            IEnumerable<MovieCharacter> movieCharacterRelation = await _context.MovieCharacters.ToListAsync();
            IEnumerable<Movie> movies = await _context.Movies.ToListAsync();

            List<Character> charactersInFranchise = new List<Character>();
            foreach (Movie m in movies)
            {
                if (m.FranchiseId == id)
                {
                    foreach (Character c in characters)
                    {
                        foreach (MovieCharacter mc in movieCharacterRelation)
                        {
                            // Prevent adding same character multiple times
                            if (m.Id == mc.MovieId && c.Id == mc.CharacterId && !charactersInFranchise.Contains(c))
                            {
                                charactersInFranchise.Add(c);
                            }
                        }
                    }
                }
            }
            return charactersInFranchise;
        }

        /// <summary>
        /// Get Franchise
        /// </summary>
        /// <param name="id">Id of Franchise</param>
        /// <returns>Async operation</returns>
        public async Task<Franchise> GetFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);

            return franchise;
        }

        /// <summary>
        /// Get Franchise
        /// </summary>
        /// <returns>Async operation</returns>
        public async Task<IEnumerable<Franchise>> GetFranchisesAsync()
        {
            return await _context.Franchises.ToListAsync();
        }

        /// <summary>
        /// Get Movies in Franchise
        /// </summary>
        /// <param name="id">Id of Franchise</param>
        /// <returns>Async operation</returns>
        public async Task<List<Movie>> GetMoviesInFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);

            IEnumerable<Movie> movies = await _context.Movies.ToListAsync();
            List<Movie> moviesInFranchise = new List<Movie>();
            foreach (Movie m in movies)
            {
                if (m.FranchiseId == id)
                {
                    moviesInFranchise.Add(m);
                }
            }
            return moviesInFranchise;
        }

        /// <summary>
        /// Post Franchise
        /// </summary>
        /// <param name="franchise">Franchise object</param>
        /// <returns>Async operation</returns>
        public async Task<Franchise> PostFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        /// <summary>
        /// Put Franchise
        /// </summary>
        /// <param name="id">Id of Franchise</param>
        /// <param name="franchise">Franchise object</param>
        /// <returns>Async operation</returns>
        public async Task PutFranchiseAsync(int id, Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Put Movies in Franchise
        /// </summary>
        /// <param name="id">Id of Franchise</param>
        /// <param name="movieIds">List of Movie Ids</param>
        /// <returns>Async operation</returns>
        public async Task<bool> PutMoviesInFranchise(int id, List<int> movieIds)
        {
            // Get Franchise
            var franchise = await _context.Franchises.FindAsync(id);
            franchise.Movies = new List<Movie>();

            // Check if all input movieIds exist in Db and add MovieId relation,
            // return false (resulting in BadRequest in controller) if not.
            List<int> validMovieIds = new();
            await _context.Movies.ForEachAsync(element => validMovieIds.Add(element.Id));
            foreach (int i in movieIds)
            {
                if (!validMovieIds.Contains(i)) return false;
                Movie m = await _context.Movies.FindAsync(i);
                m.FranchiseId = id;
                franchise.Movies.Add(m);
            }

            // Update Movie, adding respective character ids to MovieCharacter table
            _context.Entry(franchise).State = EntityState.Modified;
            
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
