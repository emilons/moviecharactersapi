﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MovieCharactersAPI.Models.Domain;

namespace MovieCharactersAPI.DataAccess
{
    public interface IFranchiseRepository
    {
        public Task<IEnumerable<Franchise>> GetFranchisesAsync();
        public Task<Franchise> GetFranchiseAsync(int id);
        public Task<List<Character>> GetCharactersInFranchise(int id);
        public Task<List<Movie>> GetMoviesInFranchise(int id);
        public Task PutFranchiseAsync(int id, Franchise franchise);
        public Task<bool> PutMoviesInFranchise(int id, List<int> movieIds);
        public Task<Franchise> PostFranchiseAsync(Franchise franchise);
        public Task DeleteFranchiseAsync(int id);
        public bool FranchiseExists(int id);
    }
}
