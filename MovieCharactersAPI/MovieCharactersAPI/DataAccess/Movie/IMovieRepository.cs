﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MovieCharactersAPI.Models.Domain;

namespace MovieCharactersAPI.DataAccess
{
    public interface IMovieRepository
    {
        public Task<IEnumerable<Movie>> GetMoviesAsync();
        public Task<Movie> GetMovieAsync(int id);
        public Task<List<Character>> GetCharactersInMovie(int id);
        public Task PutMovieAsync(int id, Movie movie);
        public Task<bool> PutCharactersInMovie(int id, List<int> characterIds);
        public Task<Movie> PostMovieAsync(Movie movie);
        public Task DeleteMovieAsync(int id);
        public bool MovieExists(int id);
    }
}
