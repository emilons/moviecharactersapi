﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models.Domain;

namespace MovieCharactersAPI.DataAccess
{
    public class MovieRepository : IMovieRepository
    {
        private readonly MovieDbContext _context;

        public MovieRepository(MovieDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Delete Movie
        /// </summary>
        /// <param name="id">Id of Movie</param>
        /// <returns>Async operation</returns>
        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Get Characters in Movie
        /// </summary>
        /// <param name="id">Id of Movie</param>
        /// <returns>Async operation</returns>
        public async Task<List<Character>> GetCharactersInMovie(int id)
        {
            var movie = await _context.Movies.FindAsync(id);

            IEnumerable<Character> characters = await _context.Characters.ToListAsync();
            IEnumerable<MovieCharacter> movieCharacterRelation = await _context.MovieCharacters.ToListAsync();
            List<Character> charactersInMovie = new List<Character>();
            foreach (Character c in characters)
            {
                foreach (MovieCharacter mc in movieCharacterRelation)
                {
                    if (id == mc.MovieId && c.Id == mc.CharacterId)
                    {
                        charactersInMovie.Add(c);
                    }
                }
            }
            return charactersInMovie;
        }

        /// <summary>
        /// Get Movie
        /// </summary>
        /// <param name="id">Id of Movie</param>
        /// <returns>Async operation</returns>
        public async Task<Movie> GetMovieAsync(int id)
        {
            var movieList = await GetMoviesAsync();
            var movie = movieList.FirstOrDefault(m => m.Id == id);
            return movie;
        }
        /// <summary>
        /// Get Movies
        /// </summary>
        /// <returns>Async operation</returns>
        public async Task<IEnumerable<Movie>> GetMoviesAsync()
        {
            return await _context.Movies
            .Include(m=>m.MovieCharacters)
            .ToListAsync();
        }

        public bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.Id == id);
        }

        /// <summary>
        /// Post Movie
        /// </summary>
        /// <param name="movie">Movie Object</param>
        /// <returns>Async operation</returns>
        public async Task<Movie> PostMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        /// <summary>
        /// Put Characters in Movie
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <param name="characterIds">List of characterIds</param>
        /// <returns>Async operation</returns>
        public async Task<bool> PutCharactersInMovie(int id, List<int> characterIds)
        {
            // Get Movie
            var movie = await _context.Movies.FindAsync(id);
            movie.MovieCharacters = new List<MovieCharacter>();

            // Check if all input characterIds exist in Db and add CharacterId relation,
            // return false (resulting in BadRequest in controller) if not.
            List<int> validCharacterIds = new();
            await _context.Characters.ForEachAsync(element => validCharacterIds.Add(element.Id));
            foreach (int i in characterIds)
            {
                if (!validCharacterIds.Contains(i)) return false;
                MovieCharacter mc = new MovieCharacter();
                mc.MovieId = id;
                mc.CharacterId = i;
                movie.MovieCharacters.Add(mc);
            }

            // Update Movie, adding respective character ids to MovieCharacter table
            _context.Entry(movie).State = EntityState.Modified;

            await _context.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Put Movie
        /// </summary>
        /// <param name="id">Id of Movie</param>
        /// <param name="movie">Movie Object</param>
        /// <returns>Async operation</returns>
        public async Task PutMovieAsync(int id, Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
