﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using MovieCharactersAPI.Models.Domain;


namespace MovieCharactersAPI.DataAccess
{
    public class MovieDbContext : DbContext
	{
		public DbSet<Movie> Movies { get; set; }
		public DbSet<Character> Characters { get; set; }
		public DbSet<Franchise> Franchises { get; set; }
		public DbSet<MovieCharacter> MovieCharacters { get; set; }
		public MovieDbContext([NotNullAttribute] DbContextOptions options) : base(options)
		{

		}
		
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			//Create many-to-many Movie and Character relation by adding linking table
			modelBuilder.Entity<MovieCharacter>().
				HasKey(movieCharacter => new { movieCharacter.MovieId, movieCharacter.CharacterId });
			
			// Create one-to-many Franchise and Movie relation by adding foreign key to Franchise
			modelBuilder.Entity<Movie>()
				.HasOne<Franchise>(movie => movie.Franchise)
				.WithMany(franchise => franchise.Movies)
				.HasForeignKey(movie => movie.FranchiseId);

			// Seed dummy data
			AddSeedingData(modelBuilder);

		}

		/// <summary>
		/// Helper method to add dummy seeding data.
		/// </summary>
		/// <param name="modelBuilder">ModelBuilder object which creates the migrations.</param>
		private void AddSeedingData(ModelBuilder modelBuilder)
        {
			// Add Movies
			modelBuilder.Entity<Movie>().HasData(new Movie
			{
				Id = 1,
				MovieTitle = "Star Wars: Episode IV - A New Hope",
				Genre = "Sci-Fi",
				ReleaseYear = 1977,
				Director = "George Lucas",
				Picture = "https://www.imdb.com/title/tt0076759/mediaviewer/rm3263717120/",
				Trailer = "https://www.youtube.com/watch?v=vZ734NWnAHA",
				FranchiseId = 1
			});

			modelBuilder.Entity<Movie>().HasData(new Movie
			{
				Id = 2,
				MovieTitle = "Star Wars: Episode V: The Empire Strikes Back",
				Genre = "Sci-Fi",
				ReleaseYear = 1980,
				Director = "Irvin Kershner",
				Picture = "https://www.imdb.com/title/tt0080684/mediaviewer/rm3114097664/",
				Trailer = "https://www.youtube.com/watch?v=JNwNXF9Y6kY",
				FranchiseId = 1
			});

			modelBuilder.Entity<Movie>().HasData(new Movie
			{
				Id = 3,
				MovieTitle = "Star Wars: Episode VI: Return of the Jedi",
				Genre = "Sci-Fi",
				ReleaseYear = 1983,
				Director = "Richard Marquand",
				Picture = "https://www.imdb.com/title/tt0086190/mediaviewer/rm602420224/",
				Trailer = "https://www.youtube.com/watch?v=7L8p7_SLzvU",
				FranchiseId = 1
			});

			modelBuilder.Entity<Movie>().HasData(new Movie
			{
				Id = 4,
				MovieTitle = "Mission: Impossible",
				Genre = "Action",
				ReleaseYear = 1996,
				Director = "Brian De Palma",
				Picture = "https://www.imdb.com/title/tt0117060/mediaviewer/rm3243234304/",
				Trailer = "https://www.youtube.com/watch?v=Ohws8y572KE",
				FranchiseId = 2
			});

			modelBuilder.Entity<Movie>().HasData(new Movie
			{
				Id = 5,
				MovieTitle = "Twilight",
				Genre = "Fantasy",
				ReleaseYear = 2008,
				Director = "Catherine Hardwicke",
				Picture = "https://www.imdb.com/title/tt1099212/mediaviewer/rm2266076160/",
				Trailer = "https://www.youtube.com/watch?v=uxjNDE2fMjI",
				FranchiseId = 3
			});


			// Add Characters
			modelBuilder.Entity<Character>().HasData(new Character
			{
				Id = 1,
				FullName = "Luke Skywalker",
				Profession = "Padawan",
				Alias = null,
				Gender = "Male",
				Picture = "https://en.wikipedia.org/wiki/Luke_Skywalker#/media/File:Luke_Skywalker.png",
			});

			modelBuilder.Entity<Character>().HasData(new Character
			{
				Id = 2,
				FullName = "Leia Organa",
				Profession = "Princess",
				Alias = null,
				Gender = "Female",
				Picture = "https://upload.wikimedia.org/wikipedia/en/1/1b/Princess_Leia%27s_characteristic_hairstyle.jpg",
			});

			modelBuilder.Entity<Character>().HasData(new Character
			{
				Id = 3,
				FullName = "Ethan Hunt",
				Profession = "Agent",
				Alias = null,
				Gender = "Male",
				Picture = "https://upload.wikimedia.org/wikipedia/en/f/f2/Ethan_Hunt_MI.jpg",
			});

			modelBuilder.Entity<Character>().HasData(new Character
			{
				Id = 4,
				FullName = "Bella Swan",
				Profession = "Student",
				Alias = null,
				Gender = "Female",
				Picture = "https://upload.wikimedia.org/wikipedia/en/7/77/Bella_Swan.jpg",
			});

			// Add Franchises
			modelBuilder.Entity<Franchise>().HasData(new Franchise
			{
				Id = 1,
				Name = "Star Wars",
				Description = "A long time ago in a galaxy far far away...",
			});

			modelBuilder.Entity<Franchise>().HasData(new Franchise
			{
				Id = 2,
				Name = "Mission Impossible",
				Description = "DUN DUN DUNDUN DUN DUN DUNDUN, DIDALEEEEEEEEEEEEE, DIDALEEEEEEE, DIDALEEEEEE, DADAH!",
			});

			modelBuilder.Entity<Franchise>().HasData(new Franchise
			{
				Id = 3,
				Name = "The Twilight Saga",
				Description = "Teenagers, vampires and werevoles and love triangles and stuff.",
			});

			// Add MovieCharacters
			modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
			{
				MovieId = 1,
				CharacterId = 1
			});

			modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
			{
				MovieId = 2,
				CharacterId = 1
			});

			modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
			{
				MovieId = 3,
				CharacterId = 1
			});

			modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
			{
				MovieId = 1,
				CharacterId = 2
			});

			modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
			{
				MovieId = 2,
				CharacterId = 2
			});

			modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
			{
				MovieId = 3,
				CharacterId = 2
			});

			modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
			{
				MovieId = 4,
				CharacterId = 3
			});

			modelBuilder.Entity<MovieCharacter>().HasData(new MovieCharacter
			{
				MovieId = 5,
				CharacterId = 4
			});
		}
	}
}

